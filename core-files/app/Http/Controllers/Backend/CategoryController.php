<?php

namespace App\Http\Controllers\Backend;

use App\Model\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();
        return view('admin.category.index')->with([
            'categories'    => $categories
        ]);
    }

    public function store(Request $request){
        
        $category = Category::create($request->only('name'));
        
        if($request->file('image')){
            $name = time().".".$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move('images/cat',$name);
            $name = 'images/cat/'.$name;
            $category->fill([
                'image'  => $name
            ])->save();
        }
        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully created'
        ]);
    }

    public function edit($id){
        $category = Category::find($id);
        return response()->json($category,200);
    }

    public function update(Request $request,$id){
        $category = Category::findOrFail($id);
        $path = public_path().'/'.$category->partner_image;
        $category->update($request->only('name'));
        if($request->file('image')){
            $name = time().".".$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move('images/cat',$name);
            $name = 'images/cat/'.$name;
            $category->fill([
                'image'  => $name
            ])->save();
            File::delete($path);
        
        }
        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully updated'
        ]);
    }

    public function delete($id){
        Category::findOrFail($id)->delete();
        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully deleted'
        ]);
    }
}
