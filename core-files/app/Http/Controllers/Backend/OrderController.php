<?php

namespace App\Http\Controllers\Backend;

use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index(Request $request){
        $dates = $request->datefilter ? explode('-',$request->date_range) : [];

        $where = [];
        if(count($dates)){
            $where[] = ['created_at','>=',Carbon::parse(trim($dates[0]))->format('Y-m-d')];
            $where[] = ['created_at','<=',Carbon::parse(trim($dates[1]))->format('Y-m-d')];
        }
        if(count($where)){
            $orders = Order::where($where)->orderBy('id','desc')->get();
        }else{
            $orders = Order::orderBy('id','desc')->get();
        }

        return view('admin.order.index')->with([
            'orders'    => $orders,
            'datefilter'    => $request->datefilter
        ]);
    }
    public function show($id){
        $order = Order::findOrFail($id);
        return view('admin.order.show')->with([
            'order' => $order
        ]);
    }
    public function createOrder(Request $request){
        //return $request->all();
        if(count($request->item)){
            $purchase_order = Order::create($request->only('name','email','contact_no','address'));
            $payable_amount = 0;
            foreach ($request->item as $item){
                $product = Product::findOrFail($item['product_id']);
                OrderDetail::create([
                    'product_id'   => $item['product_id'],
                    'image_id'   => $item['image_id'],
                    'order_id' => $purchase_order->id,
                    'sub_total' => $item['quantity'] * $product->price,
                    'price'     => $product->price,
                    'quantity'  => $item['quantity'],
                ]);
                $payable_amount += ($item['quantity'] * $product->price);
            }
            $purchase_order->fill([
                'payable_amount'    => $payable_amount
            ])->save();

            return redirect()->back();
            return response()->json([
                'status'    => 1,
                'text'      => 'Success! You order has been placed.',
                'order'     => $purchase_order
            ],200);
        }else{
            return redirect()->back();
            return response()->json([
                'status'    => 0,
                'text'      => 'Sorry! You have no item to purchase.'
            ],200);

        }
    }
    public function confirmOrder($id){
        Order::findOrFail($id)->update([
            'status'    => 1
        ]);
        return redirect()->back();
    }
    public function cancelOrder($id){
        Order::findOrFail($id)->update([
            'status'    => 0
        ]);
        return redirect()->back();
    }
    public function completeOrder($id){}
}
