<?php

namespace App\Http\Controllers\Backend;

use App\Model\Category;
use App\Model\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class ProductController extends Controller
{
    public function index(){
        $products = Product::orderBy('id','desc')->get();
        return view('admin.product.index')->with([
            'products'  => $products
        ]);
    }

    public function show($id){
        $product = Product::findOrFail($id);
        return view('admin.product.show')->with([
            'product'   => $product
        ]);
    }

    public function singleProduct($id){
        $product = Product::with('images')->where('id','=',$id)->firstOrFail();
        return response()->json($product);
    }

    public function create(){
        $categories = Category::all();
        return view('admin.product.create')->with([
            'categories'    => $categories
        ]);
    }

    public function store(Request $request){
        $product = Product::create($request->only('name','category_id','description','price','is_stock'));
        if($request->file('image')){
            $name = time().".".$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move('images/product',$name);
            $name = 'images/product/'.$name;
            $product->fill([
                'image'  => $name
            ])->save();
        }

        return redirect()->route('products')->withMessage([
            'status'    => 'alert-danger',
            'text'      => 'Successfully created product.'
        ]);
    }

    public function edit($id){
        $product = Product::findOrFail($id);
        $categories = Category::all();
        return view('admin.product.edit')->with([
            'product'   => $product,
            'categories'    => $categories
        ]);
    }

    public function update(Request $request,$id){
        $product = Product::findOrFail($id);
        $path = public_path().'/'.$product->partner_image;
        $product->update($request->only('name','category_id','description','price','is_stock'));
        if($request->file('image')){
            $name = time().".".$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move('images/product',$name);
            $name = 'images/product/'.$name;
            $product->fill([
                'image'  => $name
            ])->save();
            File::delete($path);

        }

        return redirect()->route('products')->withMessage([
            'status'    => 'alert-danger',
            'text'      => 'Successfully updated product.'
        ]);
    }

    public function delete($id){
        Product::findOrFail($id)->delete();
        return redirect()->back();
    }
}
