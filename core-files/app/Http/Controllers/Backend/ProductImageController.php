<?php

namespace App\Http\Controllers\Backend;

use App\Model\Product;
use App\Model\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductImageController extends Controller
{

    public function create(Request $request,$id){
        //return $request->all();
        $product = Product::findOrFail($id);
        if($request->file('thumb')){
            $name = time().".".$request->file('thumb')->getClientOriginalExtension();
            $request->file('thumb')->move('images/product',$name);
            $name = 'images/product/'.$name;
            ProductImage::create([
                'title' => $request->title,
                'thumb' => $name,
                'status'    => $request->status ? 1 : 0,
                'product_id'    => $product->id
            ]);
            return redirect()->back()->withMessage([
                'status'    => 'alert-success',
                'text'      => 'Image has benn attached successfully'
            ]);
        }

        return redirect()->back();
    }

    public function edit($id){
        $product_image = ProductImage::findOrFail($id);
        return response()->json($product_image,200);
    }

    public function update(Request $request,$id){
        if($request->file('thumb')){
            $name = time().".".$request->file('thumb')->getClientOriginalExtension();
            $request->file('thumb')->move('images/product',$name);
            $name = 'images/product/'.$name;
            ProductImage::findOrFail($id)->update([
                'title' => $request->title,
                'thumb' => $name,
                'status'    => $request->status ? 1 : 0,
            ]);
            return redirect()->back()->withMessage([
                'status'    => 'alert-success',
                'text'      => 'Image has benn attached successfully'
            ]);
        }else{
            ProductImage::findOrFail($id)->update([
                'title' => $request->title,
                'status'    => $request->status ? 1 : 0,
            ]);
        }

        return redirect()->back();
    }

    public function delete($id){
        ProductImage::findOrFail($id)->delete();
        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Image has benn deleted successfully'
        ]);
    }
}
