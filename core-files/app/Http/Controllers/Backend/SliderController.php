<?php

namespace App\Http\Controllers\Backend;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    public function index(){
        $sliders = Slider::orderBy('id','desc')->get();
        return view('admin.slider.index')->with([
            'sliders'   => $sliders
        ]);
    }

    public function create(Request $request){
        if($request->file('image')){
            $name = time().".".$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move('images/slider',$name);
            $name = 'images/slider/'.$name;
            Slider::create([
                'image' => $name
            ]);
        }

        return redirect()->back();
    }

    public function delete($id){
        $slider = Slider::findOrFail($id);
        $path = public_path().'/'.$slider->image;
        File::delete($path);

        Slider::findOrFail($id)->delete();
    }
}
