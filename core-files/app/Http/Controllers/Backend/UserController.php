<?php

namespace App\Http\Controllers\Backend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index(){
        $users = User::orderBy('id','desc')->get();
        return view('admin.user.index')->with([
            'users' => $users
        ]);
    }

    public function store(Request $request){
        User::create([
            'name'  => $request->name,
            'email' => $request->email,
            'password'  => bcrypt($request->password)
        ]);

        return redirect()->back();
    }

    public function edit($id){
        $user = User::findOrFail($id);
        return response()->json($user,200);
    }

    public function update(Request $request,$id){
        $user = User::findOrFail($id);
        $user->update([
            'name'  => $request->name,
            'email' => $request->email,
            'password'  => bcrypt($request->password)
        ]);
        return redirect()->back();
    }

    public function delete($id){
        User::findOrFail($id)->delete();
        return redirect()->back();
    }
}
