<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Product;
use App\Slider;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $products = Product::where('is_stock',1)->get();
        $sliders = Slider::orderBy('id','desc')->get();
        return view('welcome')->with([
            'categories'    => $categories,
            'products'      => $products,
            'sliders'       => $sliders
        ]);
    }
}
