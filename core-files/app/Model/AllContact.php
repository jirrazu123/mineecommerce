<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AllContact extends Model
{
    protected $fillable = ['email'];
}
