<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','image'];

    public function products(){
        return $this->hasMany('App\Model\Product','category_id','id');
    }
}
