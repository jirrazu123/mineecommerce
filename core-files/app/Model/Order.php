<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','email','contact_no','address','payable_amount','status'];

    public function orderDetails(){
        return $this->hasMany('App\Model\OrderDetail','order_id','id');
    }
}
