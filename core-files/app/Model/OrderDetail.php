<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = ['order_id','product_id','quantity','price','sub_total','image_id'];
    public function order(){
        return $this->belongsTo('App\Model\Order','order_id','id');
    }

    public function image(){
        return $this->belongsTo('App\Model\ProductImage','image_id','id');
    }

    public function product(){
        return $this->belongsTo('App\Model\Product','product_id','id');
    }
}
