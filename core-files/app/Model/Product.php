<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','image','category_id','description','price'];
    protected $appends = ['thumb'];

    public function category(){
        return $this->belongsTo('App\Model\Category','category_id','id');
    }

    public function getThumbAttribute(){
        return asset($this->image);
    }

    public function images(){
        return $this->hasMany('App\Model\ProductImage','product_id','id');
    }
}
