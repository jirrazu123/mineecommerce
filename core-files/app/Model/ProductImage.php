<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = ['thumb','title','status','product_id'];
    protected $appends = ['image_url'];


    public function getImageUrlAttribute(){
        return url('/'.$this->thumb);
    }

}
