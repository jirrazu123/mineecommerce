<?php
return [
    'order_status' => [
        'Order Placed',
        'Order Confirmed',
    ],
    'order_class' => [
        'default',
        'danger',
        'info',
        'success',
    ]
];