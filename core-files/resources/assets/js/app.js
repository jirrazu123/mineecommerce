
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vuex from 'vuex';
import store from './store.js';
window.Vuex = Vuex;

window.Vue = require('vue');
import Paginate from 'vuejs-paginate';
import Toasted from 'vue-toasted';
Vue.use(Toasted);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('products', require('./components/Products.vue'));
Vue.component('add-to-order-button', require('./components/AddToCartButton.vue'));
Vue.component('items', require('./components/Items.vue'));
Vue.component('item-count', require('./components/ItemCount.vue'));
Vue.component('paginate', Paginate);

const app = new Vue({
    el: '#app',
    store: new Vuex.Store(store),
});
