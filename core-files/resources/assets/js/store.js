let store = {
    state: {
        cart: [],
        cartCount: 0,

    },

    mutations: {
        addItemToCart(state, item) {
            let found = state.cart.find(product => product.id == item.id);
            for (let i=0; i<state.cart.length; i++){
                if(state.cart[i].id == item.id){
                    state.cart.splice(i,1,item);
                    return;
                }
            }

            state.cart.push(item);
            console.log(state.cart);

        },
        updateItemToCart(state, item) {
            for (let i=0; i<state.cart.length; i++){
                if(state.cart[i].id == item.id){
                    state.cart.splice(i,1,item);
                }
            }
            // let found = state.cart.find(product => product.id == item.id);
            //
            // if (found) {
            //     found.quantity = item.quantity;
            //     found.subtotal = item.subtotal;
            // } else {
            //     item.quantity = 1;
            //     state.cart.push(item);
            //     console.log(state.cart);
            // }

        },
        removeFromCart(state, item) {
            console.log("deleting");
            for(let i=0; i<state.cart.length; i++){
                if(state.cart[i].id == item.id){
                    state.cart.splice(i,1);
                }
            }

        },
        addToCart(state,items){
            state.cart = items;
            console.log("items");
            console.log(items);
        },
        saveCart(state) {
            window.localStorage.setItem('cart', JSON.stringify(state.cart));
            window.localStorage.setItem('cartCount', state.cartCount);
        },
    }
};

export default store;