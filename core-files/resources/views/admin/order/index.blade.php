@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3>
                LIST OF ORDERS
                @if($datefilter)
                    <a href="#" class="btn btn-success btn-sm pull-right">{{ $datefilter }}</a>
                @endif
            </h3>
        </div>
        <div class="box-body">
            <form action="" method="get">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <input class="form-control" type="text" name="datefilter" value="" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button class="btn btn-block" type="submit">SEARCH</button>
                        </div>
                    </div>
                </div>
            </form>
            <table id="purchase-orders-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Full Name</th>
                    <th>Contact No</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Amount</th>
                    <th class="text-right">Status</th>
                    <th class="text-right">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->name}}</td>
                        <td>{{ $order->contact_no }}</td>
                        <td>{{ $order->email }}</td>
                        <td>{{ $order->address }}</td>
                        <td>{{ $order->payable_amount }}</td>
                        <td class="text-right">
                            {{ Config::get('enums.order_status')[$order->status] }}
                        </td>
                        <td class="text-right">
                            @if(!$order->status)
                                <a href="{{ route('accept-order',['id' => $order->id]) }}" class="btn btn-xs btn-info">Confirm</a>
                            @else
                                <a href="{{ route('cancel-order',['id' => $order->id]) }}" class="btn btn-xs btn-danger">Cancel</a>
                            @endif
                                <a href="{{ route('order-details',['id' => $order->id]) }}" class="btn btn-xs btn-success">View</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {

            $("#purchase-orders-table").dataTable();

            $('input[name="datefilter"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

        });
    </script>
@endsection