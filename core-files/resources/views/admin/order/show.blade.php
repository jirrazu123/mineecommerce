@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3>
                ORDER DETAILS
            </h3>
            <table class="table">
                <tbody>
                <tr>
                    <td><strong>Order From :</strong> {{ $order->name  }}</td>
                    <td><strong>Contact No :</strong> {{ $order->contact_no  }}</td>
                    <td><strong>Delivery Address :</strong> {{ $order->address  }}</td>
                    <td><strong>Email :</strong> {{ $order->email  }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="box-body">
            <table id="purchase-orders-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Photo</th>
                    <th>Product</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->orderDetails as $k => $orderDetail)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>
                            @if(@$orderDetail->product->image)
                                <img src="{{ asset(@$orderDetail->image->thumb) }}" class="p-image" width="50" alt="" >
                            @endif
                        </td>
                        <td>{{ @$orderDetail->product->name }}<br/>{{ @$orderDetail->product->description }}<br/></td>
                        <td>{{ @$orderDetail->product->price }}</td>
                        <td>{{ @$orderDetail->quantity }}</td>
                        <td>{{ @$orderDetail->product->price * $orderDetail->quantity }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <p class="text-right" style="margin-top: 50px;">
                @if(!$order->status)
                    <a href="{{ route('accept-order',['id' => $order->id]) }}" class="btn btn-sm btn-info">Confirm</a>
                @else
                    <a href="{{ route('cancel-order',['id' => $order->id]) }}" class="btn btn-sm btn-danger">Cancel</a>
                @endif
            </p>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {


        });
    </script>
@endsection