@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3>
                CREATE PRODUCT
                <a href="{{ route('products') }}" class="btn btn-sm pull-right btn-info">
                    <i class="fa fa-plus-circle"></i> PRODUCT LIST
                </a>
            </h3>
        </div>
        <div class="box-body">
            <form action="{{ route('post.create-product') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Product Name <span class="text-danger m-l-5">*</span></label>
                                <input type="text" name="name" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Category</label>
                                <select name="category_id" class="form-control">
                                    <option value="">-- Select Category --</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" cols="30" rows="" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Price</label>
                                <input type="text" name="price" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Product List Image</label>
                                <input type="file" name="image" class="form-control">
                                <h4 class="text-danger">Image Height Should be 175x200 pixel</h4>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label><input type="checkbox" value="1" name="is_stock" checked>Available</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> Save Product</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
