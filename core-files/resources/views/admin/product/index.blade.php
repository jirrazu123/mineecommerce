@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3>
                LIST OF PRODUCTS
                <a href="{{ route('create-product') }}" class="btn btn-sm pull-right btn-info">
                    <i class="fa fa-plus-circle"></i> CREATE NEW
                </a>
            </h3>
        </div>
        <div class="box-body">
            <table id="products-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Product Name</th>
                    <th>Image</th>
                    <th>Category</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $k => $product)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td><img src="{{ asset($product->image) }}" alt="" style="width: 46px"></td>
                        <td><a href="{{ route('product',['id' => $product->id]) }}">{{ $product->name }}</a></td>
                        <td>{{ $product->category ? $product->category->name : '' }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->price }}</td>
                        <td class="text-right">
                            <a href="{{ route('product',['id' => $product->id]) }}" class="btn btn-info btn-xs fla"><i class="fa fa-eye" ></i>View Details</a>
                            <a href="{{ route('edit-product',['id' => $product->id]) }}" class="btn btn-warning btn-xs fla"><i class="fa fa-edit" ></i>Edit</a>
                            <button data-url="{{ route('delete-product',['id' => $product->id]) }}" class="btn btn-danger btn-xs flat btn-delete" data-toggle="modal" data-target="#delete-content-modal"><i class="fa fa-trash-o"></i>Delete</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#products-table').DataTable()
        });
    </script>
@endsection