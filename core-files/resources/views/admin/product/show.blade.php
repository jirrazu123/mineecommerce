@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3>
                PRODUCT DETAILS
                <a href="{{ route('products') }}" class="btn btn-sm pull-right btn-info">
                    <i class="fa fa-plus-circle"></i> PRODUCT LIST
                </a>
            </h3>
        </div>
        <hr>
        <div class="box-body">
            <div class="row">
                <div class="col-md-8">
                    <p><strong>Product Name : </strong>{{ $product->name }}</p>
                    <p><strong>Category : </strong>{{ $product->category ? $product->category->name: '' }}</p>
                    <p>Price : </strong>{{ $product->price }}</p>
                    <p><strong>Description : </strong> {{ $product->description }}</p>
                </div>
                <div class="col-md-4">
                    <img src="{{ asset($product->image) }}" alt="Product Image" style="max-width: 100%; border: 10px solid #f7f7f7;">
                </div>
            </div>
            <h3>
                PRODUCT IMAGES
                <button type="button" class="btn btn-sm pull-right btn-info" data-toggle="modal" data-target="#product-image-modal">
                    <i class="fa fa-plus-circle"></i> ADD MORE IMAGE
                </button>
            </h3>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($product->images as $k => $image)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td><img src="{{ asset($image->thumb) }}" alt="" style="width: 100px"></td>
                        <td>{{ $image->title }}</td>
                        <td>{{ $image->status ? 'Active' : 'Inactive' }}</td>
                        <td class="text-right">
                            <button data-id="{{ $image->id }}" class="btn btn-warning btn-xs flat btn-edit" data-toggle="modal" data-target="#edit-product-image"><i class="fa fa-edit" ></i>Edit</button>
                            <button data-url="{{ route('delete-product-image',['id' => $image->id]) }}" class="btn btn-danger btn-xs flat btn-delete" data-toggle="modal" data-target="#delete-content-modal"><i class="fa fa-trash-o"></i>Delete</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade in" id="product-image-modal" tabindex="-1" role="dialog" aria-labelledby="category">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('create-product-image',['id' => $product->id]) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">CREATE PRODUCT IMAGE</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title" class="control-label">Title *</label>
                            <input type="text" class="form-control" id="title" name="title" required>
                        </div>

                        <div class="form-group">
                            <label for="name" class="control-label">Product Slide Image *</label>
                            <input type="file" class="form-control" id="thumb" name="thumb">
                            <h4 class="text-danger">Image Height Should be 300x400 pixel</h4>
                        </div>
                        <div class="form-group">
                            <div class="checkbox icheck" style="padding-left:20px">
                                <label>
                                    <input type="checkbox" name="status" checked>  Active
                                </label>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm btn-flat" data-dismiss="modal">CLOSE</button>
                        <button type="submit" class="btn btn-primary btn-sm btn-flat">CREATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="edit-product-image" tabindex="-1" role="dialog" aria-labelledby="edit-product-image">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="#" id="edit-product-image-form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">UPDATE PRODUCT IMAGE</h4>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name" class="control-label">Title *</label>
                                <input type="text" class="form-control" id="title" name="title" required>
                            </div>

                            <div class="form-group">
                                <label for="name" class="control-label">Image *</label>
                                <input type="file" class="form-control" id="thumb" name="thumb">
                            </div>
                            <div class="form-group">
                                <div class="checkbox icheck" style="padding-left:20px">
                                    <label>
                                        <input type="checkbox" name="status" checked>  Active
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm btn-flat" data-dismiss="modal">CLOSE</button>
                        <button type="submit" class="btn btn-primary btn-sm btn-flat">UPDATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {

            var url = "{{ route('edit-product-image',['id' => '']) }}/";

            $(document).on('click','.btn-edit',function () {
                var id = $(this).data('id');
                var api_url = url +id;

                $.ajax({url: api_url, success: function(result){
                        $("#edit-product-image-form").attr('action',api_url);
                        $("#edit-product-image-form input[name='title']").val(result.title);
                    }});
            });

        });
    </script>
@endsection
