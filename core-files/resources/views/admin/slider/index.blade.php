@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3>
                SLIDER
            </h3>
        </div>
        <div class="box-body">
            <form action="{{ route('create-slider') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <input class="form-control" type="file" name="image" />
                            <h5 class="text-warning">Image should be 1024 x 350 pixel</h5>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button class="btn btn-info" type="submit">CREATE SLIDER</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="box-body">
            <div class="row">
                @foreach($sliders as $slider)
                    <div class="col-md-6">
                        <div class="image-thump" style="margin-bottom: 15px; padding: 10px; border: 1px solid #dddddd;">
                            <img src="{{ asset($slider->image) }}" alt="" class="img-responsive">
                            <p style="margin-top: 15px;text-align: right">
                                <a href="{{ route('delete-slider',['id' => $slider->id]) }}" class="btn btn-info btn-sm">Delete</a>
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {


        });
    </script>
@endsection