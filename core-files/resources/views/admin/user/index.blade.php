@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3>
                LIST OF CUSTOMER REPRESENTATIVE
                <button type="button" class="btn btn-sm pull-right btn-info" data-toggle="modal" data-target="#user-modal">
                    <i class="fa fa-plus-circle"></i> CREATE NEW
                </button>
            </h3>
        </div>
        <div class="box-body">
            <table id="users-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $k => $user)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <button data-id="{{ $user->id }}" class="btn btn-warning btn-xs flat btn-edit" data-toggle="modal" data-target="#edit-user"><i class="fa fa-edit" ></i>Edit</button>
                            <button data-url="{{ route('delete-user',['id' => $user->id]) }}" class="btn btn-danger btn-xs flat btn-delete" data-toggle="modal" data-target="#delete-content-modal"><i class="fa fa-trash-o"></i>Delete</button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade in" id="user-modal" tabindex="-1" role="dialog" aria-labelledby="user">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('create-user') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">CREATE USER</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="control-label">Name *</label>
                            <input type="text" class="form-control" name="name" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email(username) *</label>
                            <input type="email" class="form-control"  name="email" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Password *</label>
                            <input type="password" class="form-control" name="password" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm btn-flat" data-dismiss="modal">CLOSE</button>
                        <button type="submit" class="btn btn-primary btn-sm btn-flat">CREATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="edit-user" tabindex="-1" role="dialog" aria-labelledby="edit-user">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="#" id="edit-user-form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">UPDATE USER</h4>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name" class="control-label">Name *</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email(username) *</label>
                                <input type="email" class="form-control"  name="email" required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Password *</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm btn-flat" data-dismiss="modal">CLOSE</button>
                        <button type="submit" class="btn btn-primary btn-sm btn-flat">UPDATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {

            var url = "{{ route('edit-user',['id' => '']) }}/";

            $(document).on('click','.btn-edit',function () {
                var id = $(this).data('id');
                var api_url = url +id;

                $.ajax({url: api_url, success: function(result){
                        $("#edit-user-form").attr('action',api_url);
                        $("#edit-user-form input[name='name']").val(result.name);
                        $("#edit-user-form input[name='email']").val(result.email);
                    }});
            });

            $('#users-table').DataTable()
        });
    </script>
@endsection