<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="base-url" content="{{ url('/') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Raleway:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
    <title>{{ config('app.name', 'Raiyan') }}</title>
</head>
<body>

<header class="header">
    <div class="header-top" style="background-color: #f7f7f7">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-8">
                    <p>
                        <strong><i class="icon ion-md-call"></i>&nbsp;&nbsp;{{ $settings ? $settings->mobile : '' }}</strong>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <strong><i class="icon ion-md-mail"></i>&nbsp;&nbsp;{{ $settings ? $settings->email : '' }}</strong>
                    </p>
                </div>
                <div class="col-sm-4 text-right">
                    @if($settings && $settings->facebook)
                        <a class="btn-facebook" target="_blank" href="{{ $settings? $settings->facebook : '' }}" role="button"><img src="{{ asset('frontend/images/icons/facebook.png') }}" class="medium-icon"></a>
                    @endif
                    @if($settings && $settings->google_plus)
                        <a class="btn-google" target="_blank" href="{{ $settings ? $settings->google_plus : '' }}" role="button"><img src="{{ asset('frontend/images/icons/google-plus.png') }}" class="medium-icon"></a>
                    @endif
                    @if($settings && $settings->twitter)
                        <a class="btn-twitter" target="_blank" href="{{ $settings ? $settings->twitter : '' }}" role="button"><img src="{{ asset('frontend/images/icons/twitter.png') }}" class="medium-icon"></a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <nav id="navbar-example2" class="navbar navbar-light text-center" style="background-color: #fff; border-bottom: 3px solid #f7f7f7">
        <img src="{{ asset('frontend/images/logo.png') }}" class="w-auto" alt="Logo" style="max-height: 60px; margin: 0 auto;">
    </nav>
    <section class="banner">
        <div class="owl-carousel owl-theme">
            @foreach($sliders as $slider)
                <img src="{{ asset($slider->image) }}"  class="item">
            @endforeach
        </div>
    </section>
</header>

<main id="app">
    <section class="section-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                    <div class="item-list-container">
                        <ul class="nav nav-pills navigation-tab">
                            <li class="nav-item align-self-center">
                                <a class="nav-link active" data-toggle="tab" href="#category-all">All</a>
                            </li>
                            @foreach($categories as $k => $category)
                                <li class="nav-item align-self-center">
                                    <a class="nav-link" data-toggle="tab" href="#category-{{ $category->id }}">
                                        @if($category->image)
                                            <img src="{{ asset($category->image) }}" class="menu-image" />
                                        @else
                                            {{ $category->name }}
                                        @endif
                                    </a>
                                </li>
                            @endforeach
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active container" id="category-all">
                                <products :products="{{ $products }}"></products>
                            </div>
                            @foreach($categories as $k => $category)
                                <div class="tab-pane container" id="category-{{ $category->id }}">
                                    <products :products="{{ $category->products }}"></products>
                                </div>
                            @endforeach


                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 d-xs-none padding-none">
                    <div id="po" class="purchase-order" target="_blank">
                        <form action="{{ route('save-order') }}" method="post">
                            {{ csrf_field() }}
                            <div class="z-2">
                                <div class="customer-info">
                                    <div class="header">
                                        <h4>ORDER DETAILS</h4>
                                    </div>
                                    <div class="info">
                                        <h5 class="title">CUSTOMER INFORMATION</h5>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control custom-input" name="name" id="name" placeholder="Enter Your Full Name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control custom-input" name="contact_no" id="phone" placeholder="Mobile No" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="email" class="form-control custom-input" name="email" id="email" placeholder="Email Address (If any)">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control custom-input" name="address" id="address" placeholder="Delivery Address" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <items></items>
                            </div>

                            <button class="btn btn-o btn-block btn-submit" type="submit">SUBMIT</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="section-about">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ asset('frontend/images/about.jpg') }}" class="about-img">
                </div>
                <div class="col-md-6">
                    <div class="about-us">
                        <h3>ABOUT US</h3>
                        <p>
                            {{ $settings ? $settings->description : '' }}
                        </p>
                        <div class="blank-100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="scroll-checkout">
        <img src="{{ asset('images/fast-delivery.png') }}" class="cart-icon">
        <item-count></item-count>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="singleProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <single-product ref="getSingleProduct"></single-product>
                </div>
            </div>
        </div>
    </div>

</main>

<footer>
    <section class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3653.081767866966!2d90.40681531438373!3d23.708773884609933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6edffd143c546362!2sPatuatuli+Jame+Masjid!5e0!3m2!1sen!2sbd!4v1540903203030" width="100%" height="140" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-md-3">
                    <div class="footer-widget">
                        <h4 class="title">Address</h4>
                        <p>{{ $settings ? $settings->address : '' }}</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-widget">
                        <h4 class="title">Email</h4>
                        <p>{{ $settings ? $settings->email : '' }}</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer-widget">
                        <h4 class="title">Contact</h4>
                        <p>{{ $settings ? $settings->mobile : '' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="footer-bottom gray-bg">
        <div class="container-fluid afbl-img-container">
            <div class="row">
                <div class="col-sm-2 align-self-center br-gray-2">
                    <h4 class="footer-widget sp-title">Home Delivery Service</h4>
                </div>
                <div class="col-sm-4 align-self-center">
                    <div class="footer-widget">
                        <p class="sp-contact"><span class="icon-round"><i class="icon ion-md-call"></i></span>HOTLINE {{ $settings ? $settings->phone : '' }}</p>
                    </div>
                </div>
                <div class="col-sm-4 ml-auto text-right">
                    @if($settings && $settings->facebook)
                        <a class="btn-facebook" target="_blank" href="{{ $settings? $settings->facebook : '' }}" role="button"><img src="{{ asset('frontend/images/icons/facebook.png') }}" class="medium-icon"></a>
                    @endif
                    @if($settings && $settings->google_plus)
                    <a class="btn-google" target="_blank" href="{{ $settings ? $settings->google_plus : '' }}" role="button"><img src="{{ asset('frontend/images/icons/google-plus.png') }}" class="medium-icon"></a>
                    @endif
                    @if($settings && $settings->twitter)
                    <a class="btn-twitter" target="_blank" href="{{ $settings ? $settings->twitter : '' }}" role="button"><img src="{{ asset('frontend/images/icons/twitter.png') }}" class="medium-icon"></a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <small class="copyright">&copy; All right reserved by Raiyan 2018</small>
            </div>
            <div class="col-sm-6 text-sm-right">
                <small class="developer">
                    Designe & Develop By <a href="http://www.exabytelab.com" target="_blank"><em>Exabytelab</em></a>
                </small>
            </div>
        </div>
    </div>
</footer>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-shopping-cart"></span></a>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="{{ asset('core-files/public/js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="{{asset('frontend/js/main.js')}}"></script>
</body>
</html>