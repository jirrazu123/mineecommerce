<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// !JFF4X](?T-u

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){
    /**
     * Dashboard
     */
	Route::get('dashboard', [
		'uses' => 'Backend\DashboardController@index',
		'as'   => 'dashboard'
	]);
	/*
	 * Products
	 */
    Route::get('products',[
        'uses'  => 'Backend\ProductController@index',
        'as'    => 'products'
    ]);

    Route::get('create-product',[
        'uses'  => 'Backend\ProductController@create',
        'as'    => 'create-product'
    ]);

    Route::post('create-product',[
        'uses'  => 'Backend\ProductController@store',
        'as'    => 'post.create-product'
    ]);

    Route::get('edit-product/{id}',[
        'uses'  => 'Backend\ProductController@edit',
        'as'    => 'edit-product'
    ]);

    Route::post('update-product/{id}',[
        'uses'  => 'Backend\ProductController@update',
        'as'    => 'update-product'
    ]);

    Route::get('delete-product/{id}',[
        'uses'  => 'Backend\ProductController@delete',
        'as'    => 'delete-product'
    ]);

    /**
     * Categories
     */
    Route::get('categories',[
        'uses'  => 'Backend\CategoryController@index',
        'as'    => 'categories'
    ]);

    Route::post('create-category',[
        'uses'  => 'Backend\CategoryController@store',
        'as'    => 'create-category'
    ]);

    Route::get('update-category/{id}',[
        'uses'  => 'Backend\CategoryController@edit',
        'as'    => 'edit-category'
    ]);

    Route::post('update-category/{id}',[
        'uses'  => 'Backend\CategoryController@update',
        'as'    => 'update-category'
    ]);

    Route::get('delete-category/{id}',[
        'uses'  => 'Backend\CategoryController@delete',
        'as'    => 'delete-category'
    ]);


    /**
     * Purchase Order
     */

    Route::post('save-order',[
        'uses'  => 'Backend\OrderController@createOrder',
        'as'    => 'save-order'
    ]);

    Route::get('accept-order/{id}',[
        'uses'  => 'Backend\OrderController@confirmOrder',
        'as'    => 'accept-order'
    ]);

    Route::get('cancel-order/{id}',[
        'uses'  => 'Backend\OrderController@cancelOrder',
        'as'    => 'cancel-order'
    ]);

    Route::get('complete-order/{id}',[
        'uses'  => 'Backend\OrderController@completeOrder',
        'as'    => 'complete-order'
    ]);

    Route::get('order-list',[
        'uses'  => 'Backend\OrderController@index',
        'as'    => 'order-list'
    ]);


    /**
     * Basic Settings
     */

    Route::get('basic-settings',[
        'uses'  => 'Backend\BasicSettingsController@index',
        'as'    => 'basic-settings'
    ]);

    Route::post('basic-settings',[
        'uses'  => 'Backend\BasicSettingsController@update',
        'as'    => 'update-basic-settings'
    ]);

    Route::get('tags-json',function(){
        $tags = \App\Model\Tag::pluck('name');
        return response()->json($tags,200);
    })->name('tags-json');


	Route::get('logout', function(){
		Auth::logout();
		return redirect()->route('login');
	});
});
