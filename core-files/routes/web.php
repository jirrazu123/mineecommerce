<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// !JFF4X](?T-u

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('save-order',[
    'uses'  => 'Backend\OrderController@createOrder',
    'as'    => 'save-order'
]);

Route::get('/single-product/{id}',[
    'uses'  => 'Backend\ProductController@singleProduct',
    'as'    => 'delete-product'
]);

Route::group(['middleware' => 'auth'], function(){
    /**
     * Dashboard
     */
	Route::get('dashboard', [
		'uses' => 'Backend\DashboardController@index',
		'as'   => 'dashboard'
	]);
	Route::group(['middleware' => 'admin'],function (){
        /*
     * Products
     */
        Route::get('products',[
            'uses'  => 'Backend\ProductController@index',
            'as'    => 'products'
        ]);

        Route::get('product/{id}',[
            'uses'  => 'Backend\ProductController@show',
            'as'    => 'product'
        ]);

        Route::get('create-product',[
            'uses'  => 'Backend\ProductController@create',
            'as'    => 'create-product'
        ]);

        Route::post('create-product',[
            'uses'  => 'Backend\ProductController@store',
            'as'    => 'post.create-product'
        ]);

        Route::get('edit-product/{id}',[
            'uses'  => 'Backend\ProductController@edit',
            'as'    => 'edit-product'
        ]);

        Route::post('update-product/{id}',[
            'uses'  => 'Backend\ProductController@update',
            'as'    => 'update-product'
        ]);

        Route::get('delete-product/{id}',[
            'uses'  => 'Backend\ProductController@delete',
            'as'    => 'delete-product'
        ]);


        /**
         * Product Images
         */
        Route::post('create-product-image/{id}',[
            'uses'  => 'Backend\ProductImageController@create',
            'as'    => 'create-product-image'
        ]);

        Route::get('edit-product-image/{id}',[
            'uses'  => 'Backend\ProductImageController@edit',
            'as'    => 'edit-product-image'
        ]);

        Route::post('edit-product-image/{id}',[
            'uses'  => 'Backend\ProductImageController@update',
            'as'    => 'update-product-image'
        ]);

        Route::get('delete-product-image/{id}',[
            'uses'  => 'Backend\ProductImageController@delete',
            'as'    => 'delete-product-image'
        ]);

        /**
         * Categories
         */
        Route::get('categories',[
            'uses'  => 'Backend\CategoryController@index',
            'as'    => 'categories'
        ]);

        Route::post('create-category',[
            'uses'  => 'Backend\CategoryController@store',
            'as'    => 'create-category'
        ]);

        Route::get('update-category/{id}',[
            'uses'  => 'Backend\CategoryController@edit',
            'as'    => 'edit-category'
        ]);

        Route::post('update-category/{id}',[
            'uses'  => 'Backend\CategoryController@update',
            'as'    => 'update-category'
        ]);

        Route::get('delete-category/{id}',[
            'uses'  => 'Backend\CategoryController@delete',
            'as'    => 'delete-category'
        ]);


        /**
         * Slider
         */

        Route::get('sliders',[
            'uses'  => 'Backend\SliderController@index',
            'as'    => 'sliders'
        ]);

        Route::post('create-slider',[
            'uses'  => 'Backend\SliderController@create',
            'as'    => 'create-slider'
        ]);

        Route::get('delete-slider/{id}',[
            'uses'  => 'Backend\SliderController@delete',
            'as'    => 'delete-slider'
        ]);




        /**
         * Customer Representative
         */

        Route::get('customer-representative',[
            'uses'  => 'Backend\UserController@index',
            'as'    => 'customer-representative'
        ]);

        Route::post('create-user',[
            'uses'  => 'Backend\UserController@store',
            'as'    => 'create-user'
        ]);

        Route::get('update-user/{id}',[
            'uses'  => 'Backend\UserController@edit',
            'as'    => 'edit-user'
        ]);

        Route::post('update-user/{id}',[
            'uses'  => 'Backend\UserController@update',
            'as'    => 'update-category'
        ]);

        Route::get('delete-user/{id}',[
            'uses'  => 'Backend\UserController@delete',
            'as'    => 'delete-user'
        ]);


        /**
         * Basic Settings
         */

        Route::get('basic-settings',[
            'uses'  => 'Backend\BasicSettingsController@index',
            'as'    => 'basic-settings'
        ]);

        Route::post('basic-settings',[
            'uses'  => 'Backend\BasicSettingsController@update',
            'as'    => 'update-basic-settings'
        ]);
    });

    /**
     * Purchase Order
     */


    Route::get('accept-order/{id}',[
        'uses'  => 'Backend\OrderController@confirmOrder',
        'as'    => 'accept-order'
    ]);

    Route::get('cancel-order/{id}',[
        'uses'  => 'Backend\OrderController@cancelOrder',
        'as'    => 'cancel-order'
    ]);

    Route::get('complete-order/{id}',[
        'uses'  => 'Backend\OrderController@completeOrder',
        'as'    => 'complete-order'
    ]);

    Route::get('order-details/{id}',[
        'uses'  => 'Backend\OrderController@show',
        'as'    => 'order-details'
    ]);


    Route::get('order-list',[
        'uses'  => 'Backend\OrderController@index',
        'as'    => 'order-list'
    ]);


	Route::get('logout', function(){
		Auth::logout();
		return redirect()->route('login');
	});
});


Route::get('restore-mail',function(){
    $emails = \App\Model\Dailyenquiry::distinct('ContactPersonEmail')->pluck('ContactPersonEmail');
    //return $emails;
    foreach ($emails as $email){
        if(strlen($email) > 5){
            \App\Model\AllContact::firstOrCreate([
                'email' => $email
            ]);
        }
    }
});