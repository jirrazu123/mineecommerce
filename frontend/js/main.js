$(document).ready(function(){
    $("#scroll-checkout").click(function() {
        $('html, body').animate({
            scrollTop: $("#po").offset().top
        }, 2000);
    });

    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:false,
        responsive:{
            0:{
                items:1,
                nav:false,
                dots: false,
            },
            600:{
                items:1,
                nav:false,
                dots: false,
            },
            1000:{
                items:1,
                nav:false,
                loop:true,
                dots: false,
            }
        }
    })
});